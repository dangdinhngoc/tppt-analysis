# Copyright (c) 2014 OptoFidelity Ltd. All Rights Reserved.

from sqlalchemy.orm import joinedload
from datetime import datetime

import time
import traceback
import TPPTAnalysisSW.plotinfo as plotinfo

from .measurementdb import get_database, TestItem, TestResult
from .settings import settings
import TPPTAnalysisSW.test_refs as test_refs

# generator functions for the different images - will be filled by the decorator
# This is a directory id -> generating function
_test_generators = {}

#decorator class
class testclasscreator(object):
    """ Creates test reports and images for the test. Gets the test type id as parameter """

    _generators = {}

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        pass

    def __call__(self, f):
        global _test_generators
        for arg in self.args:
            _test_generators[arg] = f
        return f


class TestBase(object):
    """Base class for all test objects. """

    def __init__(self, test_item_row, *args, **kwargs):
        """ Initialize the common report parameters """

        # Create dictionary for testsession parameters
        self.test_item = {column.name: getattr(test_item_row, column.name) for column in test_item_row.__table__.columns}
        self.test_item['test_type_name'] = test_item_row.type.name
        self.test_id = self.test_item['id']
        # Create dictionary for test session parameters
        session = test_item_row.testsession
        self.testsession = {column.name: getattr(session, column.name) for column in session.__table__.columns}
        dut = test_item_row.dut
        self.dut = {column.name: getattr(dut, column.name) for column in dut.__table__.columns}

    @staticmethod
    def create(test_id, dbsession=None, **kwargs):
        """ Create a correct subclass for given test id """
        global _test_generators

        if dbsession is None:
            dbsession = get_database().session()

        test = dbsession.query(TestItem).options(joinedload(TestItem.testsession)).\
                                                 options(joinedload(TestItem.type)).\
                                                 filter(TestItem.id==test_id).first()

        cache = True
        test_id = str(test_id)

        if test.testtype_id in _test_generators:
            if test_id not in test_refs.testclass_refs:
                generator = _test_generators[test.testtype_id](test, **kwargs)
                test_refs.testclass_refs[test_id] = generator
                cache = False

        if test.testtype_id != 15:
            cache = False

        return test_refs.testclass_refs[test_id], cache

    def get_test_item(self, dbsession=None):
        if dbsession is None:
            dbsession = get_database().session()

        test = dbsession.query(TestItem).options(joinedload(TestItem.testsession)). \
            options(joinedload(TestItem.type)). \
            filter(TestItem.id == self.test_id).first()

        return test

    def get_test_session(self, dbsession=None):
        if dbsession is None:
            dbsession = get_database().session()

        test = dbsession.query(TestItem).options(joinedload(TestItem.testsession)). \
            options(joinedload(TestItem.testsession)). \
            filter(TestItem.id == self.test_id).first()

        return test.testsession

    def get_session_and_dutinfo(self):
        """ Gets db session and dutinfo objects. """

        dbsession = get_database().session()
        dutinfo = plotinfo.TestDUTInfo(testdut_id=self.dut['id'], dbsession=dbsession)
        return dbsession, dutinfo

    # These functions must be implemented in the child classes

    def runanalysis(self, *args, **kwargs):
        raise NotImplementedError("Method not implemented")

    def clearanalysis(self, *args, **kwargs):
        raise NotImplementedError("Method not implemented")

    def createreport(self, *args, **kwargs):
        raise NotImplementedError("Method not implemented")

    def createimage(self, *args, **kwargs):
        raise NotImplementedError("Method not implemented")

    def createcsv(self, *args, **kwargs):
        return '"This feature is not implemented yet"'
        #raise NotImplementedError("Method not implemented")

    def create_common_templateparams(self, *args, **kwargs):
        templateParams = {}
        templateParams["test_item"] = self.test_item
        templateParams["testsession"] = self.testsession
        templateParams["dut"] = self.dut
        templateParams["test_id"] = self.test_item['id']
        templateParams["test_type_name"] = self.test_item['test_type_name']
        templateParams["curtime"] = time.time()

        templateParams["settings"] = settings

        templateParams["kwargs"] = kwargs

        return templateParams

    @staticmethod
    def evaluateresult(test_id, dbsession=None, recalculate=True):

        if dbsession is None:
            session = get_database().session()
        else:
            session = dbsession

        result = None

        if recalculate:
            testclass = TestBase.create(test_id, dbsession=session)[0]

        try:

            if recalculate:
                if testclass is not None:
                    result = testclass.runanalysis()
                else:
                    # Test class not found!
                    print("Test class not found for test " + str(test_id))
                    result = "Error"

            dbresult = TestResult()
            dbresult.test_id = test_id

            if recalculate:
                dbresult.result = result
            else:
                result = "Requires recalculate"
                dbresult.result = result

            dbresult.calculated = datetime.now()
            session.query(TestResult).filter(TestResult.test_id == test_id).delete()
            session.add(dbresult)
            session.commit()

        except Exception as e:
            print(traceback.format_exc())
            result = "Error"

        if dbsession is None:
            session.close()

        return result
