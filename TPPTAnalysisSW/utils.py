# Copyright (c) 2014 OptoFidelity Ltd. All Rights Reserved.

from decimal import Decimal
from datetime import datetime

import TPPTAnalysisSW.settings as settings

class Timer(object):
    """ Timer object that can be used for performance timing """

    do_timing = False

    def __init__(self, level=1, *args, **kwargs):
        """ Initialize timer and start timing. """
        result = super(Timer, self).__init__(*args, **kwargs)
        if self.do_timing:
            self.start = datetime.now()
            self.level = level
        return result

    def Time(self, string):
        """ Print out string and elapsed time """
        if self.do_timing:
            print(">>>" + str(self.level) + " " + str(string) + " " + str(datetime.now() - self.start))

def exportcsv(query, initialstring = None, subtable=None):
    ''' exports query to a csv format as a table 
        If optional initialstring is given, the results are appended
        to it. Initialstring should end with '\n' 
        A single subtable can be put to output by giving the subtable
        reference in parent class (as string) '''
    if initialstring is None:
        csvstring = ""
    else:
        csvstring = initialstring

    # Add headers
    headers = ""
    columns = []
    colheaders = [('"' + c.name + '"') for c in query.column_descriptions[0]['type'].__table__.columns]
    headers += settings.csvchars[1].join(colheaders)
    if subtable is not None:
        subtableclass = getattr(query.column_descriptions[0]['type'], subtable).mapper.class_
        subheaders = [('"' + c.name + '"') for c in subtableclass.__table__.columns]
        headers += settings.csvchars[1] + settings.csvchars[1].join(subheaders)
        csvstring += query.column_descriptions[0]['type'].__tablename__ + (settings.csvchars[1] * len(colheaders)) + subtableclass.__tablename__ + '\n'
    csvstring += headers + '\n'

    for line in query:
        linestr = ""

        # Not iterable - only one result
        tablecolumns = line.__table__.columns
        values = [getattr(line, tc.name) for tc in tablecolumns]
        csvvalues = []
        for v in values:
            if isinstance(v, str):
                csvvalues.append('"' + v + '"')
            elif isinstance(v, float):
                csvvalues.append(str(v).replace('.', settings.csvchars[0]))
            else:
                csvvalues.append(str(v))
        linestr += settings.csvchars[1].join(csvvalues)

        if subtable is not None:
            subtablecolumns = subtableclass.__table__.columns
            for subline in getattr(line, subtable):
                subvalues = [getattr(subline, tc.name) for tc in subtablecolumns]
                if len(linestr) == 0:
                    linestr += '\n' + (settings.csvchars[1] * len(tablecolumns))
                else:
                    linestr += settings.csvchars[1]
                csvvalues = []
                for v in subvalues:
                    if isinstance(v, str):
                        csvvalues.append('"' + v + '"')
                    elif isinstance(v, float):
                        csvvalues.append(str(v).replace('.', settings.csvchars[0]))
                    else:
                        csvvalues.append(str(v))
                linestr += settings.csvchars[1].join(csvvalues)
                csvstring += linestr
                linestr = ''

        csvstring += linestr + '\n'

    return csvstring
    