# Version is in the form A.B.C.D
# A: Fixed to 5 for TnT GT
# B: UI release version.
# C: Project ID. Zero for platform.
# D: Project release version. Zero is initial release to customer.
__version__ = "5.2.0.0"
