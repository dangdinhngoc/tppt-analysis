# Copyright (c) 2014 OptoFidelity Ltd. All Rights Reserved.
from builtins import print

import cherrypy
from genshi.template import MarkupTemplate
import numpy as np

from ..testbase import TestBase, testclasscreator
from ..imagefactory import ImageFactory
from ..settings import settings
from ..utils import Timer, exportcsv
from ..info.version import Version
import TPPTAnalysisSW.measurementdb as db
import TPPTAnalysisSW.analyzers as analyzers
import TPPTAnalysisSW.plot_factory as plot_factory
import TPPTAnalysisSW.plotinfo as plotinfo


class OneFingerTapTest(TestBase):
    """ A dummy test class for use as a template in creating new test classes """

    # This is the generator function for the class - it must exist in all derived classes
    # Just update the id (dummy=99) and class name
    @staticmethod
    @testclasscreator(0)
    def create_testclass(*args, **kwargs):
        return OneFingerTapTest(*args, **kwargs)

    # Init function: make necessary initializations.
    # Parent function initializes: self.test_id, self.test_item (dictionary, contains test_type_name) and self.testsession (dictionary)
    def __init__(self, ddtest_row, *args, **kwargs):
        """ Initializes a new OneFingerTapTest class """
        return super(OneFingerTapTest, self).__init__(ddtest_row, *args, **kwargs)

    # Create CSV file from the results
    def createcsv(self, *args, **kwargs):
        ''' Create csv file from the measurements '''
        dbsession = db.get_database().session()
        query = dbsession.query(db.OneFingerTapTest).filter(db.OneFingerTapTest.test_id == self.test_id). \
            order_by(db.OneFingerTapTest.id)
        return exportcsv(query)

    # Override to make necessary analysis for test session success
    def runanalysis(self, *args, **kwargs):
        """ Runs the analysis, return a string containing the test result """
        results = self.read_test_results()
        verdict = "Pass" if (results['max_input_verdict'] and results['missing_inputs_verdict'] and results[
            'min_input_verdict']) else "Fail"
        if settings['edgelimit'] >= 0:
            if not results['missing_edge_inputs_verdict']:
                verdict = "Fail"

        return verdict

    # Override to make necessary operations for clearing test results
    # Clearing the test result from the results table is done elsewhere
    def clearanalysis(self, *args, **kwargs):
        """ Clears analysis results """
        ImageFactory.delete_images(self.test_id)

    # Create the test report. Return the created HTML, or raise cherrypy.HTTPError
    def createreport(self, *args, **kwargs):

        self.clearanalysis()

        # Create common template parameters (including test_item dictionary, testsession dictionary, test_id, test_type_name etc)
        templateParams = super(OneFingerTapTest, self).create_common_templateparams(**kwargs)

        t = Timer()

        # data for the report
        results = self.read_test_results()
        templateParams['results'] = results

        t.Time("Results")

        # set the content to be used
        templateParams['test_page'] = 'test_one_finger_tap.html'
        templateParams['version'] = Version

        template = MarkupTemplate(open("templates/test_common_body.html"))
        stream = template.generate(**(templateParams))
        t.Time("Markup")

        # verdict = "Pass" if (results['min_input_verdict'] else "Fail"
        verdict = "Pass" if (results['max_input_verdict'] and results['missing_inputs_verdict'] and results[
            'min_input_verdict']) else "Fail"
        if settings['edgelimit'] >= 0:
            if not results['missing_edge_inputs_verdict']:
                verdict = "Fail"

        return stream.render('xhtml'), verdict

    # Create images for the report. If the function returns a value, it is used as the new image (including full path)
    def createimage(self, imagepath, image_name, *args, **kwargs):

        if image_name == 'p2pdiff':
            t = Timer(1)
            dbsession = db.get_database().session()
            dutinfo = plotinfo.TestDUTInfo(testdut_id=self.dut['id'], dbsession=dbsession)
            results = self.read_test_results(dutinfo=dutinfo, dbsession=dbsession)
            t.Time("Results")
            title = 'Preview: One Finger Tap ' + self.dut['program']
            plot_factory.plot_taptest_on_target(imagepath, results, dutinfo, *args, title=title, **kwargs)
            t.Time("Image")
        elif image_name == 'p2pdxdy':
            t = Timer(1)
            results = self.read_test_results(**kwargs)
            t.Time("Results")
            plot_factory.plot_dxdy_graph(imagepath, results, 0.0, *args, **kwargs)
            t.Time("Image")
        elif image_name == 'p2pdxdyltd':
            t = Timer(1)
            results = self.read_test_results(**kwargs)
            t.Time("Results")
            plot_factory.plot_dxdy_graph(imagepath, results, 1.0, *args, **kwargs)
            t.Time("Image")
        elif image_name == 'p2pdxdyltdc':
            t = Timer(1)
            results = self.read_test_results(center_only=True)
            t.Time("Results")
            plot_factory.plot_dxdy_graph(imagepath, results, 1.0, *args, center_only=True, **kwargs)
            t.Time("Image")
        elif image_name == 'p2pdxdyltde':
            t = Timer(1)
            results = self.read_test_results(edge_only=True)
            t.Time("Results")
            plot_factory.plot_dxdy_graph(imagepath, results, 1.0, *args, edge_only=True, **kwargs)
            t.Time("Image")
        elif image_name == 'p2phistogram':
            t = Timer(1)
            results = self.read_test_results()
            t.Time("Results")
            plot_factory.plot_p2p_err_histogram(imagepath, results, 1.0, *args, edge_only=False, **kwargs)
            t.Time("Image")

        return None

    def read_test_results(self, dutinfo=None, dbsession=None, **kwargs):
        if dbsession is None:
            dbsession = db.get_database().session()
        if dutinfo is None:
            dutinfo = plotinfo.TestDUTInfo(testdut_id=self.dut['id'], dbsession=dbsession)

        query = dbsession.query(db.OneFingerTapTest).filter(db.OneFingerTapTest.test_id == self.test_id). \
            order_by(db.OneFingerTapTest.id)

        passed_points = []  # These are  -tuples: ((target_x, target_y), (hit_x, hit_y))
        failed_points = []
        targets = []
        hits = []  # target points: ((target_x, target_y), radius)
        missing = []  # target points ((target_x, target_y), radius)
        missing_edge = []
        edge_points = 0
        max_input_error = analyzers.round_dec(0.0)
        min_input_error = 10000  # ngocdd3
        max_edge_error = analyzers.round_dec(0.0)
        distances = []

        # Pass/Fail Conditions
        # ngocdd3
        edge_standard = settings['edgepositioningerror']
        center_standard = settings['maxposerror']
        score = 100
        total_points = []
        # Pass/Fail Conditions

        # find number of edge and center points
        for point in query:
            target1 = analyzers.robot_to_target((point.robot_x, point.robot_y), dutinfo)
            total_points.append(target1)

        score_per_point = score / len(total_points)

        for point in query:
            target = analyzers.robot_to_target((point.robot_x, point.robot_y), dutinfo)
            targets.append(target)

            edge_point = False
            if analyzers.is_edge_point(target, dutinfo):
                edge_point = True
                edge_points += 1
                if 'center_only' in kwargs:
                    continue
            else:
                if 'edge_only' in kwargs:
                    continue

            if point.panel_x is None or point.panel_y is None:
                missing.append((target, analyzers.get_max_error(target, dutinfo)))
                score = score - score_per_point
                if edge_point:
                    missing_edge.append((target, analyzers.get_max_error(target, dutinfo)))
            else:
                max_error = analyzers.round_dec(
                    analyzers.get_max_error(target, dutinfo))  # find point is edge or center area to get standard
                hits.append((target, max_error))
                hit = analyzers.panel_to_target((point.panel_x, point.panel_y), dutinfo)
                distance = analyzers.round_dec(np.linalg.norm((hit[0] - target[0], hit[1] - target[1])))
                # print(distance)
                distances.append(float(distance))

                # define pass/fail points follow standard
                if distance > max_error:
                    failed_points.append((target, hit))
                else:
                    passed_points.append((target, hit))

                # define edge points and center points
                if edge_point:
                    if distance > max_edge_error:
                        max_edge_error = distance
                else:
                    if distance > max_input_error:
                        max_input_error = distance
                # Evalute score for Tap Accuracy
                if edge_point:
                    if distance > edge_standard:
                        #ngocdd3
                        deviation = (float(distance) / float(edge_standard)) - 1
                        temp_score = score_per_point * deviation
                        score = int(score - temp_score)

                else:
                    if distance > center_standard:
                        #ngocdd3
                        deviation = (float(distance) / float(center_standard)) - 1
                        temp_score = score_per_point * deviation
                        score = int(score - temp_score)


        max_input_verdict = (max_input_error < settings['maxposerror'])
        min_input_verdict = (min_input_error < settings['maxposerror'])  # ngocdd3

        if settings['edgelimit'] >= 0:
            max_input_verdict = max_input_verdict and max_edge_error < settings['edgepositioningerror']

        results = {'max_input_error': max_input_error,
                   'min_input_error': min_input_error,  # ngocdd3
                   'edge_analysis_done': (settings['edgelimit'] >= 0),
                   'max_edge_error': max_edge_error,
                   'max_input_verdict': max_input_verdict,
                   'min_input_verdict': min_input_verdict,  # ngocdd3
                   'total_points': len(targets),
                   'edge_points': edge_points,
                   'missing_inputs': len(missing),
                   'missing_inputs_verdict': (len(missing) - len(missing_edge) <= settings['maxmissing']),
                   'missing_edge_inputs': len(missing_edge),
                   'missing_edge_inputs_verdict': (len(missing_edge) <= settings['maxedgemissing']),
                   'passed_points': passed_points,
                   'failed_points': failed_points,
                   'targets': targets,
                   'maxposerror': settings['maxposerror'],
                   'hits': hits,
                   'missing': missing,
                   'distances': distances,
                   'score': int(score)
                   }

        if settings['edgelimit'] >= 0:
            # Edge analysis
            results['edgepositioningerror'] = settings['edgepositioningerror']
            results['images'] = [(ImageFactory.create_image_name(self.test_id, 'p2pdiff'),
                                  ImageFactory.create_image_name(self.test_id, 'p2pdiff', 'detailed')),
                                 (ImageFactory.create_image_name(self.test_id, 'p2pdxdy'),
                                  ImageFactory.create_image_name(self.test_id, 'p2pdxdy', 'detailed')),
                                 (ImageFactory.create_image_name(self.test_id, 'p2pdxdyltdc'),
                                  ImageFactory.create_image_name(self.test_id, 'p2pdxdyltdc', 'detailed')),
                                 (ImageFactory.create_image_name(self.test_id, 'p2pdxdyltde'),
                                  ImageFactory.create_image_name(self.test_id, 'p2pdxdyltde', 'detailed')),
                                 (ImageFactory.create_image_name(self.test_id, 'p2phistogram'),
                                  ImageFactory.create_image_name(self.test_id, 'p2phistogram', 'detailed'))]
        else:
            results['images'] = [(ImageFactory.create_image_name(self.test_id, 'p2pdiff'),
                                  ImageFactory.create_image_name(self.test_id, 'p2pdiff', 'detailed')),
                                 (ImageFactory.create_image_name(self.test_id, 'p2pdxdy'),
                                  ImageFactory.create_image_name(self.test_id, 'p2pdxdy', 'detailed')),
                                 (ImageFactory.create_image_name(self.test_id, 'p2pdxdyltd'),
                                  ImageFactory.create_image_name(self.test_id, 'p2pdxdyltd', 'detailed')),
                                 (ImageFactory.create_image_name(self.test_id, 'p2phistogram'),
                                  ImageFactory.create_image_name(self.test_id, 'p2phistogram', 'detailed'))]

        return results
