# Copyright (c) 2014 OptoFidelity Ltd. All Rights Reserved.

from decimal import Decimal
from .measurementdb import Setting

settings = {}

precision = Decimal("0.001") # Precision to use in calculations

setting_categories = {'One Finger First Contact Latency': ['maxactiveresponselatency', 'maxidleresponselatency'],
                      'One Finger Non-Stationary Reporting Rate': ['minreportingrate'],
                      'One Finger Swipe': ['maxoffset', 'maxjitter', 'jittermask', 'maxmissingswipes'],
                      'One Finger Tap': ['edgelimit', 'maxposerror', 'maxmissing', 'edgepositioningerror', 'maxedgemissing'],
                      'One Finger Tap Repeatability': ['maxrepeaterror'],
                      'One Finger Stationary Jitter': ['maxstationaryjitter'],
                      'One Finger Stationary Reporting Rate': ['minreportingrate'],
                      'MultiFinger Tap': ['edgelimit', 'maxposerror', 'maxmissing', 'edgepositioningerror', 'maxedgemissing'],
                      'MultiFinger Swipe': ['maxoffset', 'maxjitter', 'jittermask', 'maxmissingswipes'],
                      'Two Finger Separation': ['maxseparation', 'maxdiagseparation'],
                      # 'Edge Grid Suppression':['maxdistance'] # ngocdd3
                      }

csvchars = ".,"

def loadSettings(dbsession):
    global settings
    dbSettings = dbsession.query(Setting).all()

    for s in dbSettings:
        settings[s.id] = Decimal.quantize(Decimal(s.value), precision)
